#!/usr/bin/env bats

@test "can run the script" {
    ./fizzbuzz
}

@test "line 3 is Fizz" {
    ./fizzbuzz | sed '3q;d'   | grep Fizz
}

@test "line 5 is Buzz" {
    ./fizzbuzz | sed '5q;d'   | grep Buzz
}

@test "line 15 is FizzBuzz" {
    ./fizzbuzz | sed '15q;d'   | grep FizzBuzz
}


