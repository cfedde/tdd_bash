---
theme: gaia
paginate: true

---
# Test Driven Development

- Chris Fedde
- Chris@fedde.us

---
# Test Driven Development

An Example in Bash

* How to generate confidence in your code
* By writing twice as much code

---
# Test Driven Development

An Example in Bash

* FizzBuzz in Bash
* And a test suite to convince ourselves that it works
  * Bash Automated Testing System
  * ([bats-core](https://github.com/bats-core/bats-core))

---
# FizzBuzz

* An old interview coding question
* Write a program that plays the game FizzBuzz
  * Start by counting from 1 to 100
  * If the number is divisible by 3 print "Fizz"
  * If the number is divisible by 5 print "Buzz"
  * if the number is divisible by both 3 and 5 print "FizzBuzz"

---
# Development Environment

* Checkout from https://gitlab.com/cfedde/tdd_bash
* Two terminal windows
  * Evaluation window where we run the code
  * Editing window where we write the code

---
# Design -- App


```
1
2
3
4
5
6
...
15
...
98
99
100
```
---
# Design -- App

```
1
2
Fizz
4
Buzz
Fizz
...
FizzBuzz
...
98
Fizz
Buzz
```

---
# Design -- Test

* How am I going to convince myself that the code does the right
  thing?
  * Does the app even run?
  * Are there 100 lines?
  * Is the third line "Fizz"?
  * Is the fifth line "Buzz"?
  * Is the fifteenth  line "FizzBuzz"?

---
# The Demo

---
# The License 

To the extent possible this code is released to the
public domain

---
# Questions?

```
$ echo "questions?" | figlet
                       _   _                ___
  __ _ _   _  ___  ___| |_(_) ___  _ __  __|__ \
 / _` | | | |/ _ \/ __| __| |/ _ \| '_ \/ __|/ /
| (_| | |_| |  __/\__ \ |_| | (_) | | | \__ \_|
 \__, |\__,_|\___||___/\__|_|\___/|_| |_|___(_)
    |_|
```
