# tdd_bash

A simple demo of TDD using bash

## Slides

Find [slides](https://cfedde.gitlab.io/tdd_bash/Slides.html) at this
url: https://cfedde.gitlab.io/tdd_bash/Slides.html

These slides are using [Marp](https://marp.app/). To translate markdown
into a standalone HTML slides file use a cli like this:

```bash
marp Slides.md
```

Then load the Slides.html into a browser.

## Notes

Releases: https://gitlab.com/cfedde/tdd_bash/-/releases

### Needful Packages

- git
- bash
- [bats-core](https://github.com/bats-core/bats-core)
- vim
- inotify-tools
